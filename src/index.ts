import express from 'express';
import io from 'socket.io';
import { info } from './helper/log';
import { SWARMMQ_PORT } from './helper/environment';
import { startDiscovery } from './discovery';
import { IncomingConnection } from './connections/incoming.connection';

export async function startBroker() {
	const server = express().listen(SWARMMQ_PORT, () => {
		info(`Listening on port ${SWARMMQ_PORT}.`);
	});

	io(server, { origins: '*:*' }).on('connection', (socket) => {
		new IncomingConnection(socket);
	});
	startDiscovery();
}

startBroker();
