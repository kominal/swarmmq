import io from 'socket.io-client';
import { Packet } from './models/packet';
import { SWARMMQ_BROKER, SWARMMQ_PORT } from './helper/environment';

export class SMQClient {
	public topics: string[] = [];
	public queues: string[] = [];

	private socket: SocketIOClient.Socket;

	constructor(config: { onConnect: () => Promise<void>; onDisconnect: () => Promise<void>; onMessage: (packet: Packet) => Promise<void> }) {
		this.socket = io(`http://${SWARMMQ_BROKER}:${SWARMMQ_PORT}`, {
			transports: ['websocket'],
		});

		this.socket.on('connect', () => config.onConnect());
		this.socket.on('disconnect', () => config.onDisconnect());
		this.socket.on('MESSAGE', (packet: Packet) => config.onMessage(packet));
	}

	subscribe(destinationType: 'TOPIC' | 'QUEUE', destinationName: string) {
		if (destinationType === 'TOPIC') {
			if (!this.topics.includes(destinationName)) {
				this.topics.push(destinationName);
			}
		} else if (destinationType === 'QUEUE') {
			if (!this.queues.includes(destinationName)) {
				this.queues.push(destinationName);
			}
		}
		const packet: Packet = {
			destinationType,
			destinationName,
		};
		this.socket.emit('SUBSCRIBE', packet);
	}

	unsubscribe(destinationType: 'TOPIC' | 'QUEUE', destinationName: string) {
		if (destinationType === 'TOPIC') {
			if (this.topics.includes(destinationName)) {
				this.topics.splice(this.topics.indexOf(destinationName), 1);
			}
		} else if (destinationType === 'QUEUE') {
			if (this.queues.includes(destinationName)) {
				this.queues.splice(this.queues.indexOf(destinationName), 1);
			}
		}
		const packet: Packet = {
			destinationType,
			destinationName,
		};
		this.socket.emit('UNSUBSCRIBE', packet);
	}

	publish(destinationType: 'TOPIC' | 'QUEUE', destinationName: string, type?: string, body?: any) {
		const packet: Packet = {
			destinationType,
			destinationName,
			event: type ? { type, body } : undefined,
		};
		this.socket.emit('PUBLISH', packet);
	}
}
