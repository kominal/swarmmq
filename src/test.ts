import { SMQClient } from './client.smq';

const client = new SMQClient({
	onConnect: async () => {
		console.log('Connected!');
	},
	onDisconnect: async () => {
		console.log('Disconnected!');
	},
	onMessage: async (packet) => {
		console.log(`Received message: `, packet);
	},
});

client.subscribe('TOPIC', 'test_topic');
client.subscribe('QUEUE', 'test_queue');

client.publish('TOPIC', 'test_topic', 'Test topic message');
client.publish('QUEUE', 'test_queue', 'Test queue message', 'Test body');
