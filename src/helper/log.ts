export enum Level {
	UNKNOWN = 'UNKNOWN',
	DEBUG = 'DEBUG',
	INFO = 'INFO',
	LOG = 'LOG',
	WARN = 'WARN',
	ERROR = 'ERROR',
}

function print(level: Level, consoleMessage: string) {
	if (!level) {
		level = Level.UNKNOWN;
	}

	let color = '';
	if (level === Level.DEBUG) {
		color = '\x1b[36m';
	} else if (level === Level.INFO) {
		color = '\x1b[32m';
	} else if (level === Level.WARN) {
		color = '#ffd700';
	} else if (level === Level.ERROR) {
		color = '\x1b[31m';
	}

	console.log(`${now()} | ${color}${level}\x1b[0m > ${consoleMessage}`);
}

function now() {
	return new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
}

export function debug(consoleMessage: string) {
	print(Level.DEBUG, consoleMessage);
}

export function log(consoleMessage: string) {
	print(Level.LOG, consoleMessage);
}

export function info(consoleMessage: string) {
	print(Level.INFO, consoleMessage);
}

export function warn(consoleMessage: string) {
	print(Level.WARN, consoleMessage);
}

export function error(consoleMessage: string) {
	print(Level.ERROR, consoleMessage);
}
