export const SWARMMQ_BROKER = process.env.SWARMMQ_BROKER != null ? process.env.SWARMMQ_BROKER : 'swarmmq';
export const SWARMMQ_PORT = process.env.SWARMMQ_PORT != null ? Number(process.env.SWARMMQ_PORT) : 3000;
