import { error } from './helper/log';
import dns from 'dns';
import { SWARMMQ_BROKER } from './helper/environment';
import { OutgoingConnection } from './connections/outgoing.connection';

export const outgoingConnections: OutgoingConnection[] = [];

export async function startDiscovery() {
	try {
		await runDiscovery();
	} catch (e) {
		error(e);
	}

	setTimeout(startDiscovery, 10000);
}

export async function runDiscovery() {
	const addresses = await findTasks();
	addresses
		.filter((address) => !outgoingConnections.find((connection) => connection.address === address))
		.forEach((address) => outgoingConnections.push(new OutgoingConnection(address)));
}

export async function findTasks(): Promise<string[]> {
	return new Promise<string[]>((resolve, reject) => {
		dns.resolve4(`tasks.${SWARMMQ_BROKER}`, (e, addresses) => {
			if (e) {
				reject();
			}
			resolve(addresses);
		});
	});
}
