export interface Packet {
	destinationType: 'TOPIC' | 'QUEUE';
	destinationName: string;
	event?: { type: string; body?: any };
}
