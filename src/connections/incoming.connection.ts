import { Socket } from 'socket.io';
import { Packet } from '../models/packet';
import { info } from '../helper/log';
import { outgoingConnections } from '../discovery';

export const incomingConnections: IncomingConnection[] = [];

export class IncomingConnection {
	public type: 'BROKER' | 'CONSUMER' = 'CONSUMER';
	public topics: string[] = [];
	public queues: string[] = [];

	constructor(public socket: Socket) {
		incomingConnections.push(this);

		socket.on('DISCOVERY', () => this.onDiscovery());
		socket.on('disconnect', () => this.onDisconnect());

		socket.on('PUBLISH', (packet) => this.onPublish(packet));

		socket.on('SUBSCRIBE', (packet) => this.onSubscribe(packet));
		socket.on('UNSUBSCRIBE', (packet) => this.onUnsubscribe(packet));
	}

	onDiscovery() {
		this.type = 'BROKER';

		info(`Cluster now has ${incomingConnections.filter((c) => c.type === 'BROKER').length} broker(s).`);

		const topics: string[] = [];
		const queues: string[] = [];
		incomingConnections
			.filter((c) => c.type === 'CONSUMER')
			.forEach((c) => {
				for (const name of c.topics) {
					if (!topics.includes(name)) {
						topics.push(name);
					}
				}
				for (const name of c.queues) {
					if (!queues.includes(name)) {
						queues.push(name);
					}
				}
			});

		topics.forEach((destinationName) => this.onSubscribe({ destinationType: 'TOPIC', destinationName }));
		queues.forEach((destinationName) => this.onSubscribe({ destinationType: 'QUEUE', destinationName }));
	}

	onDisconnect() {
		if (incomingConnections.includes(this)) {
			incomingConnections.splice(incomingConnections.indexOf(this), 1);
		}

		const topics = [...this.topics];
		const queues = [...this.queues];

		topics.forEach((destinationName) => this.onUnsubscribe({ destinationType: 'TOPIC', destinationName }));
		queues.forEach((destinationName) => this.onUnsubscribe({ destinationType: 'QUEUE', destinationName }));
	}

	onSubscribe(packet: Packet) {
		const { destinationName, destinationType } = packet;

		if (!destinationName || !destinationType) {
			info(`Received invalid packet: ${JSON.stringify(packet)}`);
			return;
		}

		if (destinationType === 'QUEUE') {
			if (!this.queues.includes(destinationName)) {
				this.queues.push(destinationName);

				if (this.type === 'CONSUMER') {
					outgoingConnections.forEach((c) => c.socket.emit('SUBSCRIBE', packet));
				}
			}
		} else if (destinationType === 'TOPIC') {
			if (!this.topics.includes(destinationName)) {
				this.topics.push(destinationName);

				if (this.type === 'CONSUMER') {
					outgoingConnections.forEach((c) => c.socket.emit('SUBSCRIBE', packet));
				}
			}
		}
	}

	onUnsubscribe(packet: Packet) {
		const { destinationName, destinationType } = packet;

		if (!destinationName || !destinationType) {
			info(`Received invalid packet: ${JSON.stringify(packet)}`);
			return;
		}

		if (destinationType === 'QUEUE') {
			if (this.queues.includes(destinationName)) {
				this.queues.splice(this.queues.indexOf(destinationName), 1);
			}

			if (this.type === 'CONSUMER' && !incomingConnections.find((c) => c.type === 'CONSUMER' && c.queues.includes(destinationName))) {
				outgoingConnections.forEach((c) => c.socket.emit('UNSUBSCRIBE', packet));
			}
		} else if (destinationType === 'TOPIC') {
			if (this.topics.includes(destinationName)) {
				this.topics.splice(this.topics.indexOf(destinationName), 1);
			}

			if (this.type === 'CONSUMER' && !incomingConnections.find((c) => c.type === 'CONSUMER' && c.topics.includes(destinationName))) {
				outgoingConnections.forEach((c) => c.socket.emit('UNSUBSCRIBE', packet));
			}
		}
	}

	onPublish(packet: Packet) {
		const { destinationName, destinationType, event } = packet;

		if (!destinationName || !destinationType || !event) {
			info(`Received invalid packet: ${JSON.stringify(packet)}`);
			return;
		}

		if (destinationType === 'QUEUE') {
			const consumer = incomingConnections.find((c) => c.type === 'CONSUMER' && c.queues.includes(destinationName));
			if (consumer) {
				consumer.socket.emit('MESSAGE', packet);
			} else {
				const broker = incomingConnections.find((c) => c.type === 'BROKER' && c.queues.includes(destinationName));
				if (broker) {
					broker.socket.emit('MESSAGE', packet);
				} else {
					info(`Could not deliver message: ${JSON.stringify(packet)}`);
				}
			}
		} else if (destinationType === 'TOPIC') {
			incomingConnections
				.filter((c) => c.type === 'BROKER' && c.topics.includes(destinationName))
				.forEach((c) => c.socket.emit('MESSAGE', packet));
		}
	}
}
