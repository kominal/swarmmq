import { info, debug } from '../helper/log';
import io from 'socket.io-client';
import { SWARMMQ_PORT } from '../helper/environment';
import { Packet } from '../models/packet';
import { incomingConnections } from './incoming.connection';
import { outgoingConnections } from '../discovery';

export class OutgoingConnection {
	public socket: SocketIOClient.Socket;

	constructor(public address: string) {
		this.socket = io(`http://${this.address}:${SWARMMQ_PORT}`, {
			transports: ['websocket'],
		});

		this.socket.on('connect', () => this.onConnect());
		this.socket.on('disconnect', () => this.onDisconnect());

		this.socket.on('MESSAGE', (packet: Packet) => this.onMessage(packet));
	}

	onConnect() {
		info(`Established connection to broker at '${this.address}'.`);
		this.socket.emit('DISCOVERY');
	}

	onDisconnect() {
		info(`Lost connection to broker at '${this.address}'.`);
		if (outgoingConnections.includes(this)) {
			outgoingConnections.splice(outgoingConnections.indexOf(this), 1);
		}
	}

	onMessage(packet: Packet) {
		const { destinationName, destinationType, event } = packet;

		if (!destinationName || !destinationType || !event) {
			info(`Received invalid packet: ${JSON.stringify(packet)}`);
			return;
		}

		if (destinationType === 'TOPIC') {
			incomingConnections
				.filter((c) => c.type === 'CONSUMER' && c.topics.includes(destinationName))
				.forEach((c) => c.socket.emit('MESSAGE', packet));
		} else if (destinationType === 'QUEUE') {
			const consumer = incomingConnections.find((c) => c.type === 'CONSUMER' && c.queues.includes(destinationName));
			if (consumer) {
				consumer.socket.emit('MESSAGE', packet);
			} else {
				info(`Could not deliver message to local consumer: ${JSON.stringify(packet)}`);
			}
		}
	}
}
